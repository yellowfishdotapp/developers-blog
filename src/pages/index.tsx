import React from "react";
import {graphql, Link as GatsbyLink} from "gatsby";
import Layout from "../components/Layout";
import {Link, Box, Flex, Heading, Text} from "@chakra-ui/react";
import SEO from "../components/SEO";

const BlogPostThumbnail = ({post}) => {
  return (
    <Box
      as="article"
      maxWidth={["100%", "100%", "24%"]}
    >
      <Heading size="sm">
        <Link as={GatsbyLink} to={post.frontmatter.path}>
          {post.frontmatter.title}
        </Link>
        <br />
        {/* <Link as={GatsbyLink} to="/blog/setting-up-this-blog"> */}
        {/*   {post.frontmatter.path} */}
        {/* </Link> */}
      </Heading>
      <small>{post.frontmatter.author}, {post.frontmatter.date}</small>
      <Text textAlign="justify">
        {post.excerpt}
      </Text>
    </Box>
  );
};

const IndexPage = ({data}) => {
  const { title, description } = data.site.siteMetadata;
  const { posts } = data.blog;

  return (
    <Layout>
      <SEO title="Home" />
      <Heading size="md">What is yellowfish?</Heading>
      <Text>
        The mission of yellowfish.app is to provide linguistic educational
        content under a free license in the public domain. The platform
        should be open-source and crowdsourced content-wise similar to Wikipedia
        if possible.
        <br />
        <Link as={GatsbyLink} float="right" to="/mission">more...</Link>
      </Text>

      <Box>
        <Heading
          size="sm"
          margin="15px 0"
          display="inline-block"
        >
          <Link as={GatsbyLink} to="/blog">
            Recent posts
          </Link>
        </Heading>
        <Flex width="100%" justify="space-between" flexDir="row">
          {posts.slice(0, 5).map((post:any) => <BlogPostThumbnail key={post.id} post={post}/>)}
        </Flex>
      </Box>
    </Layout>
  );
};

export default IndexPage;

export const pageQuery = graphql`
  query MetadataQuery {
    site {
      siteMetadata {
        title
        description
      }
    }
    blog: allMarkdownRemark {
      posts: nodes {
        frontmatter {
          path
          date(fromNow: true)
          title
          author
        }
        excerpt
        id
      }
    }
  }
`;

import * as React from "react";
import { Link as GatsbyLink } from "gatsby";
import { Link, Heading, Flex } from "@chakra-ui/react";
import Layout from "../components/Layout";
import SEO from "../components/SEO";

const NotFoundPage = () => {
  return (
    <Layout>
      <SEO title="404" />
      <Heading textAlign="center">404 Page not found</Heading>
      <Flex align="center" justify="center">
        <Link as={GatsbyLink} to="/">
          Go home
        </Link>
      </Flex>
    </Layout>
  );
};

export default NotFoundPage;

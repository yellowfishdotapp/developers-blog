import React from "react";
import {Heading, ListItem, Text, UnorderedList} from "@chakra-ui/react";
import Layout from "../components/Layout";
import SEO from "../components/SEO";

const About = () => {
  return (
    <Layout>
      <SEO title="About" />
      <Heading size="md">About me</Heading>
      <Text>
        Hi, my name is <b>An</b>. I like to think of myself first and foremost as
        just someone who likes to be creative, arbitray in medium.  I enjoy
        puzzles, music, sports and technology.
      </Text>

      <Text>
        I think honesty is the most important virtue, which is how I'll conduct
        my business decisions regarding this project as well.
      </Text>

      <Text>
        Thank you for taking the time to engage with me on this project.
      </Text>

      <Heading size="md">About this blog</Heading>
      <Text>
        This blog is made using the following stack:
      </Text>
      <UnorderedList>
        <ListItem>Gatsby js</ListItem>
        <ListItem>Chakra UI</ListItem>
      </UnorderedList>
    </Layout>
  );
};

export default About;

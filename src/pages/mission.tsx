import {Heading, ListItem, Text, UnorderedList} from "@chakra-ui/react";
import React from "react";
import Layout from "../components/Layout";
import SEO from "../components/SEO";

const Mission = () => {
  return (
    <Layout>
      <SEO title="Mission" description="Yellowfish' mission statement"/>
      <Heading as="h1" size="lg">Our Mission Statement</Heading>
      <Text textAlign="justify">
        The mission of yellowfish.app is to provide linguistic educational
        content under a free license in the public domain. The platform
        should be open-source and crowdsourced content-wise similar to Wikipedia
        if possible.
      </Text>

      <Heading as="h2" size="md">Informal personal motivation</Heading>
      <Heading as="h3" size="sm">Knowledge & education</Heading>
      <Text textAlign="justify">
        My inspiration for this project is first and foremost the massive impact
        that Wikipedia has had on the world over the past decades. To put into
        perspective how much is available: most of my computing science curriculum
        can be found in totality on Wikipedia, albeit difficult to decipher at
        times, it is there for everyone who wants to read it.
        This is where yellowfish' mission begins: to empower people by providing
        means needed to understand all the available content.
      </Text>

      <Heading as="h3" size="sm">Immigration & refugees</Heading>
      <Text textAlign="justify">
        This notion is highly personal for me, as my parents are immigrants who
        even struggle to read in their own language, let alone in a foreign
        language. The world around them constantly changing drastically with no
        means for them to grasp how it affects their reality. Concrete examples
        would be the digitisation of money or public services. I wish for
        yellowfish to ease the integration process of refugees by becoming a
        solid, all-encapsulating, one-stop education source for any language.
      </Text>

      <Heading as="h3" size="sm">Culture</Heading>
      <Text textAlign="justify">
        Another reason for this project is simply just that I love culture,
        which is inherently tied to the way people express themselves; language.
        It's intriguing to me how some expressions simply cannot be translated
        into another language. Some semantics will always be lost in translation
        which implies that there is some higher, more abstract concept that
        transcends that which we are able to express (the word emotion comes to
        mind while I'm writing this). Thus, the expressions of a language reflect
        in a way the culture of a population, their way of thinking. This
        is interesting to me because delving into other cultures gives me something
        to contrast my own reality with, it gives me perspective. It highlights
        the uniqueness of the world, which makes life interesting to me.
        As such, I find it of utmost importance to preserve the diversity of
        languages throughout the world, which is why I wish for yellowfish
        to aid in preserving languages.
      </Text>

      <Heading as="h3" size="sm">The geek inside me</Heading>
      <Text textAlign="justify">
        This aptitude with languages of mine coincidentally (or maybe not) ties
        into my computing science background where syntax and semantics are
        analysed to express computing instructions. Linguistics, computer science
        and mathematics converge in formal language theory. Besides the grammar,
        it is also exciting to think about how to model this project. How to model
        the database structure in such a way so that I can capture not only
        latin languages, but every one in the world? I look forward to what will
        come out of this.
      </Text>

      <Heading as="h3" size="sm">Career</Heading>
      <Text textAlign="justify">
        Lastly, for good measure of disclosement of my intentions, this project
        will also be the showcase of my software engineering abilities as well
        as being educational in working with production code / deployment. I
        intend to go all-out in regards to scalability, security, testing etc.
      </Text>
    </Layout>
  );
};

export default Mission;

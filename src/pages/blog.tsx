import React, {FC} from "react";
import { graphql, Link as GatsbyLink} from "gatsby";
import Layout from "../components/Layout";
import {Link} from "@chakra-ui/layout";
import {Box, Heading, Text} from "@chakra-ui/react";
import SEO from "../components/SEO";

const Blog = ({data}) => {
  const { edges: posts } = data.allMarkdownRemark;

  return (
    <Layout>
      <SEO title="Blog" />
      {posts
        .filter(post => post.node.frontmatter.title.length > 0)
        .map(({ node: post }) => {
          return (
            <Box as="article" key={post.id}>
              <Heading size="sm">
                <Link as={GatsbyLink} to={post.frontmatter.path}>
                  {post.frontmatter.title}
                </Link>
              </Heading>
            <Text fontSize="sm" m="0">{post.frontmatter.author} - {post.frontmatter.date}</Text>
            <Text textAlign="justify">{post.excerpt}</Text>
            </Box>
          );
        })
      }
    </Layout>
  );
};

export default Blog;

export const pageQuery = graphql`
  query MyQuery {
    allMarkdownRemark(sort: { order: DESC, fields: [frontmatter___date] }) {
      edges {
        node {
          excerpt(pruneLength: 250)
          id
          frontmatter {
            title
            author
            date(formatString: "MMMM DD, YYYY")
            path
          }
        }
      }
    }
  }
`;

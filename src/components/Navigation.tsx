import {MoonIcon, SunIcon} from "@chakra-ui/icons";
import {Link} from "gatsby";
import {
  Box, Flex, IconButton, ListItem, UnorderedList, useColorMode, 
  useColorModeValue
} from "@chakra-ui/react";
import {Location} from "@reach/router";
import React from "react";

const NavigationItem = ({to, label, location}) => {
  const bgColor = useColorModeValue("brand.yellow", "rgba(255, 255, 255, 0.1)");
  return (
    <ListItem
      display="inline"
      padding="5px 10px"
      margin="0 5px 0 0"
      backgroundColor={to === location ? bgColor: "none"}
      _hover={{bgColor: bgColor}}
    >
      <Box
        as={Link}
        to={to}
        sx={{ a: { borderBottom: "none", _hover: { backgroundColor: "none" } }}}
      >
        {label}
      </Box>
    </ListItem>
  );
};

const Navigation = () => {
  const { colorMode, toggleColorMode } = useColorMode();
  return (
    <Flex
      as="nav"
      flexDir="row"
      align="center"
    >
      <Box
        as={Location}
        flex="1 0 auto"
      >
        {(locationProps: any) =>
          <UnorderedList styleType="none" p="0" m="0">
            <NavigationItem
              to="/"
              label="home"
              location={locationProps.location.pathname} />
            <NavigationItem
              to="/mission"
              label="mission"
              location={locationProps.location.pathname} />
            <NavigationItem
              to="/blog"
              label="blog"
              location={locationProps.location.pathname} />
            <NavigationItem
              to="/about"
              label="about"
              location={locationProps.location.pathname} />
          </UnorderedList>
        }
      </Box>
      <Box flex="0 0 auto">
        <IconButton
          float="right"
          aria-label="Switch light/dark theme"
          variant="unstyled"
          icon={colorMode === "light" ? <MoonIcon /> : <SunIcon />}
          onClick={toggleColorMode}
          position="relative"
          top="-3px"
        />
      </Box>
    </Flex>
  );
};

export default Navigation;

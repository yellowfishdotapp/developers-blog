import React from "react";
import {Box, Flex, Heading} from "@chakra-ui/layout";
import {Text, UnorderedList, useColorMode, useColorModeValue} from "@chakra-ui/react";
import Navigation from "./Navigation";


const Layout = ({ children }) => {
  const bgColor = useColorModeValue("brand.yellow", "rgba(255, 255, 255, 0.1)");
  return (
    <Box as="main" maxWidth="800px" m="auto auto">
      <Flex
        flexDir="row"
        align="center"
        borderBottomColor={bgColor}
        borderBottomWidth="5px"
        borderBottomStyle="solid"
        justify="space-between"
        mb="15px"
        wrap="wrap"
      >
        <Heading
          display="block"
          size="md"
        >
          Yellowfish Development Blog
        </Heading>
        <Navigation />
      </Flex>
      <Text fontSize="sm" textAlign="right">Notice: This blog is still under active development!</Text>
      {children}
      <Box as="footer">
        <Text fontSize="0.9em" float="right">
          created by Quoc An Ha
        </Text>
      </Box>
    </Box>
  );
};

export default Layout;

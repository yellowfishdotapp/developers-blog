---
path: "/blog/setting-up-this-blog"
title: Setting up this blog
author: Quoc An Ha
date: 2021-06-25T19:02:30.000Z
---

Hi there,

Let me first begin by noting that this is my first time writing a blog, so my
prose might be off at times. My intent with this blog is initially to provide
insight into my thought process, as well as to document it.

For this blog, I wanted something that was easy to maintain and simple in
complexity. I settled on using [Gatsby](https://www.gatsbyjs.com) since it allows me to
write view in [React](https://www.reactjs.org), which I will also be using as the rendering
framework for the main project. Additionally, Gatsby is a static site generator
which means that the blog will consist of static HTML pages, which is useful for
loading speed and SEO. As this blog is simple of nature, these characteristics
of Gatsby make it a sound choice.

In order to speed up development, I opted to use the [ChakraUI](https://www.chakra-ui.com)
component library, which includes light/dark mode out of the box, as well with
excellent theming options.

These blog posts itself are written in Markdown, utilising the Gatsby plugin
called `gatsby-transformer-remark` to transform the markdown text into HTML.
This however wasn't enough, since the parsing resulted into actual HTML tags
such as `html±<a href="/">link</a>`, which contain no styling due to Chakra's CSSReset.
In order to have Remark transform the markdown into Chakra's components, I used
`rehype-react` and manually mapped each basic HTML component to the desired
Chakra component.

```tsx
// https://github.com/rehypejs/rehype-react/issues/24#issuecomment-794362068
const processor = unified().use(rehypeReact, {
  createElement: React.createElement,
  components: {
    a: Link,
    p: (props: any) => <Text textAlign="justify">{props.children}</Text>,
    h1: (props: any) => <Heading size="4xl">{props.children}</Heading>,
    h2: (props: any) => <Heading size="2xl">{props.children}</Heading>,
    h3: (props: any) => <Heading size="lg">{props.children}</Heading>,
    h4: (props: any) => <Heading size="md">{props.children}</Heading>,
    h5: (props: any) => <Heading size="sm">{props.children}</Heading>,
    h6: (props: any) => <Heading size="xs">{props.children}</Heading>,
    img: Image,
    ul: UnorderedList,
    li: ListItem,
    ol: OrderedList,
    table: Table,
    thead: Thead,
    tbody: Tbody,
    th: Th,
    td: Td,
    tr: Tr,
    blockquote: (props: any) => (
      <Box borderLeft="solid gray 5px" pl="1em">
        <i>{props.children}</i>
      </Box>
    ),
    hr: Divider,
    code: Code,
  },
});

const renderAst = (ast: any): JSX.Element => {
  return processor.stringify(ast) as unknown as JSX.Element;
};
```

See [this resource](https://using-remark.gatsbyjs.org/custom-components/#enabling-the-component)
for more information.

Additionally, I added [Prism](https://prismjs.com/) to have code syntax
highlighting used in the previous block. In order to get the required CSS rules
applied on the generated html, I made use of the [sx prop](https://chakra-ui.com/docs/features/the-sx-prop)
provided by ChakraUI.

```tsx
<Box
  sx={{
    "& .gatsby-highlight-code-line": {
      backgroundColor: "#444444",
      display: "block",
      marginRight: "-1em",
      marginLeft: "-1em",
      paddingRight: "1em",
      paddingLeft: "1em",
      borderLeft: "-.25em solid #f99",
    },
    "& .gatsby-highlight": {
      backgroundColor: "#2d2d2d",
      borderRadius: "0.3em",
      margin: "0.5em 0",
      padding: "1em",
      overflowY: "initial",
      overflowX: "auto",
    },
    '& .gatsby-hightlight pre[class*="language-"]': {
      backgroundColor: "transparent",
      margin: "0",
      padding: "0",
      overflow: "initial",
      float: "left",
      minWidth: "100%",
    },
    '& .gatsby-highlight pre[class*="language-"].line-numbers': {
      padding: "0",
      paddingLeft: "2.8em",
      overflow: "initial",
    },
  }}
>
  {renderAst(post.htmlAst)}
</Box>
```

Even though I was just looking for a quick solution to get this blog up and
running, exploring the Gatsby ecosystem has been a blast so far and I'm getting
too absorbed into this rabbit hole, so I'm cutting myself off now before I lose
my grasp on reality.

Thanks for reading!

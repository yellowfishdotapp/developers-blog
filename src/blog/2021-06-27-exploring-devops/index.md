---
path: "/blog/exploring-devops"
title: Exploring DevOps
author: Quoc An Ha
date: 2021-06-27T14:32:30.103Z
---

Warning: This blogpost is honestly all over the place without any sense of
direction and I want to apologize for that. This post is more of a process
and resource documenting post than one to read casually.

With the idea of Yellowfish still materialising within the confines of my mind,
I've now set up the barebones of this blog. Currently I'm manually `rsync`ing
the build output of my local development build to my VPS, which works but is
obviously not the most flexible and fail-safe approach. Specifically there are
two main issues:

1. I don't have a backup of my source in case my laptop fails.
2. Manual deployment is prone to errors.

This takes me to my next step: choosing my distributed version control solution.

# Version Control Solutions

At it's core all available solutions use [Git](https://git-scm.com/), so the
distinction between them lies mostly in the tooling / ecosystem that come
accompany them. For example, choosing [Github](https://www.github.com/) provides access to
[Actions](https://docs.github.com/en/actions) as CI/CD solutions and
[Atlassian](https://www.atlassian.com/)'s [Bitbucket](https://bitbucket.org/)
obviously works well with all their other products. However, I really prefer
FOSS if it's available, and seeing how this project will also be OSS (also free if
possible), it makes sense to choose a solution that shares that vision. Hence my
decision to use [Gitlab](https://gitlab.com/), which is open source and well
equipped to do anything which Github or Atlassian provides.

# Operations

I have created a public group on Gitlab's online platform available at
[https://gitlab.com/yellowfishdotapp/](https://gitlab.com/yellowfishdotapp/)
which will be where all public repositories related to this project will be hosted.

## Continuous Deployment

In order to get some hands on experience with CI/CD and to ease the blogging
process I'll set up continuous deployment for this [Developers Blog](https://gitlab.com/yellowfishdotapp/developers-blog)
project first. This blog being written in markdown files, this means I can
theoretically use Gitlab's web editor / IDE to write drafts and posts wherever
I am! That's really cool if you ask me.

### First approach

Gitlab continuous integration is set up really easy. Any repository can be
processed by simply adding a file named `.gitlab-ci.yml` into the root folder.
This file, which contains job instructions are then carried out according to
the logic specified.

For this project we need two stages, a build stage and a deploy stage.
The build stage simply needs to be able to run `npm install && npm build` and
the deploy stage should make an `rsync` call equivalent to what I'm doing
manually now. This brings up the issue of credentials, since currently I'm
remote `rsync`ing using my personal SSH keypair, the job executor will require
similar access. Which requires that I somehow provide a SSH private key to the
job executor, which means I should first create a new user on my VPS with
rights limited to read/write on the www folder from where the blog is served.
I'll then insert the private key into Gitlab's CI/CD variables, which can then
be used inside the build script. I ended up with the following script:

```yml
stages:
  - build
  - deploy

cache:
  paths:
    - node_modules/

build-job:
  stage: build
  image: node
  script:
    - npm install
    - npm run build

  artifacts:
    paths:
      - public/*

deploy-production-job:
  stage: deploy
  image: alpine
  before_script:
    - ls -al
    - apk update && apk upgrade && apk add --no-cache rsync openssh-client-default ca-certificates
    - update-ca-certificates
    - mkdir -p ~/.ssh
    - eval $(ssh-agent -s)
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
  script:
    - rsync -avz --delete public/* $PRODUCTION_SERVER
```

Resources:

- [https://docs.gitlab.com/ee/ci/quick_start/](https://docs.gitlab.com/ee/ci/quick_start/)
- [https://www.gatsbyjs.com/docs/recipes/gitlab-continuous-integration/](https://www.gatsbyjs.com/docs/recipes/gitlab-continuous-integration/)
- [https://docs.gitlab.com/ee/ci/ssh_keys/#using-ssh-keys-with-gitlab-cicd](https://docs.gitlab.com/ee/ci/ssh_keys/#using-ssh-keys-with-gitlab-cicd)

However, with this approach I am storing a private SSH key with access to my
server, on Gitlab. Now this sets off all my alarm bells as far as bad practices
go, so I decided to do some research into the security aspects of this method.

### Security risks versus other approaches

Gitlab CI/CD Variables are stored in a database encrypted with `aes-256-cbc`,
which can only be read using some master secret ([link](https://docs.gitlab.com/ee/ci/variables/index.html#cicd-variable-security)).
Aside from encryption, anyone with access to the pipeline can simply echo out
this variable. Access to the pipeline is defined by access to the repository,
as the pipeline is defined by the `.gitlab-ci.yml` file within the root directory.

This is a problem in my case, since this would mean that everyone with access
to pushing / merging code would be able to echo out the private key. Gitlab's
security model regarding this attack vector is to use [protected branches](https://docs.gitlab.com/ee/user/project/protected_branches.html), which restricts the people allowed to push. However this still wouldn't
definitely work for me since this model requires that the set of people I trust
altering the code equals the set of people I trust with the private key and thus
by extension access to the VPS. I can see it happening in the future that I'll
trust collaberators to accept pull/merge requests, but that doesn't necessarily
mean I'll trust them with the private key too. Now of course I could limit the
access of this private key to limit the scope of a potential attack, but I would
first want to do an inventory of my other options.

After researching online through epic Google-fu and Reddit-fu, I've come to the
conclusion that it is probably inevitable to have this risk due to the process
that I'm setting up. My demands here is that some service can deploy to my
server after building the code, in other words <b>pushing</b> the application
to the server. In order for this push to happen, some exposure of authentication
is inevitable; how else is the CI going to access the server machine?

A different approach which would mitigate this risk, and probably be better
practice security wise, is to use a <b>pull model</b>. This means that there
should be some mechanism on the production server that pulls artifacts, or build
files, and deploys the application on it's own. This can be done by routinely
polling the repository for changes (but puts a load on the repo server), or
by writing a [socat script](https://unix.stackexchange.com/questions/314550/how-to-set-a-script-to-execute-when-a-port-receives-a-message/314552#314552)
that listens for a tcp message and triggers the deploy routine. Additionally,
a full blown solution can be used by using [Kubernetes](https://kubernetes.io)
in conjunction with [Argo](https://argoproj.github.io/argo-cd/) or [Flux](https://fluxcd.io/).
However interesting K8s is though, it is outside of my scope right now for CI.

Another approach seperate from a push/pull model is to have the Gitlab runner
simply running on the production server itself. It would circumvent the need
of accessing an external server to publish the application. However, as of now,
due to all the crypto attacks on CI, I'll refrain from this approach since it
would require additional research into what other risks this would bring.
E.g. how hardened is the sandboxing of a Gitlab runner?

Lastly, any push model would be greatly enhanced by using a secret manager like
[HasiCorp's Vault](https://vaultproject.io), which handles rotating SSH keys
and provides auditing of it's use.

### Securing a pushing deploy approach

Considering all options, the simplest would be to go with the script above due to
the simplicity in setup. Security is a trade-off after all between practicality
and security and in this case for things to go epicly wrong, Gitlab will have
to fall first, which is highly unlikely. Going this route is not only 'alright'
because of Gitlab having a secure status, but also because the additional
measures to limit the scope of an attack are quite effective.

First I should set up the CI runners to only run on protected branches, which are
branches where changes have to be approved by me, so there is nobody who can
maliciously echo the private key. Additionally I'll mark the variables to be
masked so they are regex replaced to [*****] anywhere that they are used.

On the VPS itself, I should restrict the SSH key using `command=...` in `authorized_keys`
so that it can only `rsync` to a specific folder by using `rrsync`
(note the double 'r', which makes for 'restricted rsync') in addition to
disabling the login shell.

Lastly I should also setup the VPS to send me a notification email everytime the key
is used, so that I can quickly detect unauthorized access.

### Verdict

After some thinking and consideration I just wouldn't be comfortable with sharing
a SSH private key to the cloud, so I opted to go for a pull model instead, which
I'll detail in a future blogpost.

Thanks for reading!

test commit pls ignore

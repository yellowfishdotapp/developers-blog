import React from "react";
import { graphql, Link as GatsbyLink } from "gatsby";
import {
  Image,
  Heading,
  Text,
  Link,
  Divider,
  UnorderedList,
  ListItem,
  OrderedList,
  Table,
  Thead,
  Tbody,
  Td,
  Tr,
  Th,
  Box,
  Code,
} from "@chakra-ui/react";
import Layout from "../components/Layout";
import rehypeReact from "rehype-react";
import unified from "unified";
import SEO from "../components/SEO";

interface ChildrenProps {
  children?: React.ReactNode;
  node: any;
};

const MyComponent = (props: ChildrenProps) => <div>{props.children}</div>;

export default function BlogPost({ data }) {
  const { markdownRemark: post } = data;

  console.log(MyComponent({node: "asdf"}));

  // https://github.com/rehypejs/rehype-react/issues/24#issuecomment-794362068
  const processor = unified()
    .use(rehypeReact, {
      createElement: React.createElement,
      components: {
        a: Link,
        p: (props: any) => <Text textAlign="justify">{props.children}</Text>,
        h1: (props: any) => <Heading as="h2" size="lg">{props.children}</Heading>,
        h2: (props: any) => <Heading as="h3" size="md">{props.children}</Heading>,
        h3: (props: any) => <Heading as="h4" size="xs">{props.children}</Heading>,
        h4: (props: any) => <Heading as="h5" size="2xs">{props.children}</Heading>,
        h5: (props: any) => <Heading as="h6" size="3xs">{props.children}</Heading>,
        h6: (props: any) => <Heading as="h6" size="3xs">{props.children}</Heading>,
        img: Image,
        ul: UnorderedList,
        li: ListItem,
        ol: OrderedList,
        table: Table,
        thead: Thead,
        tbody: Tbody,
        th: Th,
        td: Td,
        tr: Tr,
        blockquote: (props: any) => (
          <Box borderLeft="solid gray 5px" pl="1em">
            <i>{props.children}</i>
          </Box>
        ),
        hr: Divider,
        code: Code,
      }});

  const renderAst = (ast: any): JSX.Element => {
    return (processor.stringify(ast) as unknown) as JSX.Element;
  }

  return (
    <Layout>
      <SEO title={post.frontmatter.title} article />

      <Heading as="h1" size="xl">{post.frontmatter.title}</Heading>
      <Text fontSize="sm">
        {post.frontmatter.author} - {post.frontmatter.date}
      </Text>

      <Box
        sx={{
          "& .gatsby-highlight-code-line": {
            backgroundColor: "#444444",
            display: "block",
            marginRight: "-1em",
            marginLeft: "-1em",
            paddingRight: "1em",
            paddingLeft: "1em",
            borderLeft: "-.25em solid #f99",
          },
          "& .gatsby-highlight": {
            backgroundColor: "#2d2d2d",
            borderRadius: "0.3em",
            margin: "0.5em 0",
            padding: "1em",
            overflowY: "initial",
            overflowX: "auto"
          },
          '& .gatsby-hightlight pre[class*="language-"]': {
            backgroundColor: "transparent",
            margin: "0",
            padding: "0",
            overflow: "initial",
            float: "left",
            minWidth: "100%",
          },
          '& .gatsby-highlight pre[class*="language-"].line-numbers': {
            padding: "0",
            paddingLeft: "2.8em",
            overflow: "initial",
          },
        }}
      >
        {renderAst(post.htmlAst)}
      </Box>
      <Box textAlign="center">
        <Link as={GatsbyLink} to="/blog">
          Back to overview
        </Link>
      </Box>
    </Layout>
  );
}

export const pageQuery = graphql`
  query BlogPostByPath($path: String!) {
    markdownRemark(frontmatter: { path: { eq: $path } }) {
      htmlAst
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        path
        title
        author
      }
    }
  }
`;

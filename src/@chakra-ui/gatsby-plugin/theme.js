import { extendTheme } from "@chakra-ui/react";
import { mode } from "@chakra-ui/theme-tools";

const theme = extendTheme({
  config: {
    initialColorMode: "dark",
  },
  colors: {
    brand: {
      // yellow: "#ffe200",
      yellow: "#fff07f",
      gray: "#1d1f24",
    },
  },
  styles: {
    global: (props) => ({
      body: {
        padding: "15px",
        bg: mode("white", "brand.gray")(props),
      },
    }),
  },
  components: {
    Link: {
      baseStyle: (props) => ({
        borderBottomColor: mode("black", "brand.yellow")(props),
        borderBottomWidth: "1px",
        borderBottomStyle: "dashed",
        _hover: {
          backgroundColor: mode(
            "rgba(255, 226, 0, 0.5)",
            "rgba(255, 255, 255, 0.1)"
          )(props),
          textDecoration: "none",
        },
      }),
    },
    Heading: {
      baseStyle: (props) => ({
        color: mode("black", "brand.yellow")(props),
      }),
    },
    Text: {
      baseStyle: {
        margin: "0 0 1em 0",
      },
    },
  },
});

console.log("theme.js loaded.");

export default theme;
